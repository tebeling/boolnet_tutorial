# Greetings !

### Welcome to our BoolNet Tutorial


This is a compressed collection of the functionalities of the BoolNet R- package for boolean
networks developed by Christoph Muessel, Martin Hopfensitz, Hans A. Kestler.
 

For a detailed Review see the vignette of the BoolNet R-package